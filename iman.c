#include "headers.h"

char *
trim_html(const char *input_string) {
    regex_t regex;
    regmatch_t matches[1];
    char *output_string = malloc(strlen(input_string) + 1);
    int output_pos = 0, input_pos = 0;

    if (regcomp(&regex, "<[^>]*>", 0) < 0) {
        simple_error("Failed to compile regular expression");
        return output_string;
    }

    while (regexec(&regex, input_string + input_pos, 1, matches, 0) == 0) {
        strncpy(output_string + output_pos, input_string + input_pos,
                matches[0].rm_so);
        output_pos += matches[0].rm_so;
        input_pos += matches[0].rm_eo;
    }
    strcpy(output_string + output_pos, input_string + input_pos);
    regfree(&regex);
    return output_string;
}

char *
fetch_page(char *command) {
    char *content = (char *)calloc(IO_BUFFER, sizeof(char));
    char page_path[PATH_MAX];
    strcpy(page_path, "/?topic=");
    strcat(page_path, command);
    strcat(page_path, "&section=all");

    // this is found when command doesn't exist
    char *hostname = "man.he.net";

    char request[2 * PATH_MAX];
    sprintf(request,
            "GET %s HTTP/1.1\r\n"
            "Host: %s\r\n"
            "Content-Type: text/plain\r\n\r\n",
            page_path, hostname);

    // initialize a socket for getaddrinfo()
    struct addrinfo *hints =
        (struct addrinfo *)calloc(1, sizeof(struct addrinfo));
    struct addrinfo *result; // linked list result

    hints->ai_family = AF_INET; // IPv4
    hints->ai_socktype = SOCK_STREAM;
    hints->ai_protocol = IPPROTO_TCP; // tcp

    if (getaddrinfo(hostname, "http", hints, &result) != 0) {
        free(hints);
        perror(ERROR "iMan:" RESET);
        return content;
    }

    int socket_fd = 0;
    // connect the first of the results
    for (struct addrinfo *curr = result; curr; curr = curr->ai_next) {

        // make a TCP socket connection
        socket_fd =
            socket(curr->ai_family, curr->ai_socktype, curr->ai_protocol);
        if (socket_fd != -1) {
            // try to establish connection to the socket
            if (connect(socket_fd, curr->ai_addr, curr->ai_addrlen) != -1) {

                // put a post request
                if (write(socket_fd, request, PATH_MAX - 1) < 0) {
                    simple_error("Error couldn't send request");
                    break;
                }

                // read from socket
                FILE *socket_file = fdopen(socket_fd, "r");
                char buffer[IO_BUFFER];
                size_t index = 0;
                while (fgets(buffer, IO_BUFFER - 1, socket_file)) {
                    if (index > 400) {
                        strcat(content, "\n");
                        break;
                    }
                    strcat(content, buffer);
                    index += 1;
                }
                break;
            }
            // close the socket
            close(socket_fd);
        } else {
            perror(ERROR "socket" RESET);
            exit(EXIT_FAILURE);
        }
    }

    freeaddrinfo(result);
    free(hints);
    return trim_html(content);
}

char *
iman(char *sub_command, int *exit_status) {
    input_file(sub_command);
    FILE *output_fd = output_file(sub_command);
    char *output = (char *)calloc(IO_BUFFER, sizeof(char));
    char **argv = (char **)calloc(MAX_ARGS, sizeof(char *));
    size_t argc = parse_arguments(sub_command, argv);

    if (argc < 2) {
        simple_error("Usage: iMan <command>");
        *exit_status = 2;
        goto cleanup;
    }

    strcpy(output, fetch_page(argv[1]));
    if (strcmp(output, "") == 0) {
        simple_error("ERROR\tNo such command");
        *exit_status = 2;
        goto cleanup;
    }

    if (output_fd != NULL) {
        fprintf(output_fd, "%s", output);
        strcpy(output, "");
    }

cleanup:
    free(argv);
    return output;
}

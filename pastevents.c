#include "headers.h"

#define STORAGE_BUFFER 4097

char hist_file_path[PATH_MAX];

// pastevents - global variable for number of lines
bool
delete_first_line(const char *input) {

    char my_input[PATH_MAX];
    strcpy(my_input, input);
    strcat(my_input, "\n");

    char hist_array[16][STORAGE_BUFFER];
    uint lines = 0;

    FILE *hist_fd = fopen(hist_file_path, "r");
    if (hist_fd == NULL) {
        return true;
    }

    // count numbr of lines
    char buffer[STORAGE_BUFFER];
    while (fgets(buffer, STORAGE_BUFFER - 1, hist_fd) && lines < 16) {
        strcpy(hist_array[lines++], buffer);
    }

    if (lines > 0 && strcmp(my_input, hist_array[lines - 1]) == 0) {
        // command already exists no need to add
        return false;
    }

    fclose(hist_fd);

    if (lines != 15) {
        return true;
    }

    hist_fd = fopen(hist_file_path, "w");
    if (hist_fd == NULL) {
        simple_error("Couldn't open file for writing");
        return false;
    }

    for (size_t i = 1; i < lines; i++) {
        fprintf(hist_fd, "%s", hist_array[i]);
    }

    fclose(hist_fd);
    return true;
}

// store input in histfile
void
store_input(const char *input) {

    // check if input is valid
    if (is_command(input, "log") || strlen(input) == 0) {
        return;
    }

    if (strcmp(hist_file_path, "") == 0) {
        strcpy(hist_file_path, homedir);
        strcat(hist_file_path, "/.bish_history");
    }

    bool proceed_flag = delete_first_line(input);
    if (proceed_flag == false) {
        return;
    }

    FILE *hist_fd = fopen(hist_file_path, "a");
    if (hist_fd == NULL) {
        simple_error("Error opening hist_file");
        return;
    }
    fprintf(hist_fd, "%s\n", input);
    fclose(hist_fd);
}

char *
pastevents(char *sub_command, int *exit_status) {
    input_file(sub_command);
    FILE *output_fd = output_file(sub_command);
    char *output = (char *)calloc(IO_BUFFER, sizeof(char));
    char **argv = (char **)calloc(MAX_ARGS, sizeof(char *));
    size_t argc = parse_arguments(sub_command, argv);

    if (strcmp(hist_file_path, "") == 0) {
        strcpy(hist_file_path, homedir);
        strcat(hist_file_path, "/.bish_history");
    }

    char hist_array[16][STORAGE_BUFFER];
    uint lines = 0;

    FILE *hist_fd = fopen(hist_file_path, "r");
    if (hist_fd == NULL) {
        simple_error("Error opening hist_file");
        goto cleanup;
    }

    // count numbr of lines
    char buffer[STORAGE_BUFFER];
    while (fgets(buffer, STORAGE_BUFFER - 1, hist_fd)) {
        strcpy(hist_array[lines++], buffer);
    }

    fclose(hist_fd);

    if (argc == 1) {
        for (size_t i = 0; i < lines; i++)
            strcat(output, hist_array[i]);
    } else if ((argc == 2) && (strcmp(argv[1], "purge") == 0)) {
        remove(hist_file_path);
    } else if ((argc == 3) && (strcmp(argv[1], "execute") == 0)) {
        int index = atoi(argv[2]);

        index = lines - index;
        if (!(index >= 0 && index < 15)) {
            *exit_status = 3;
            simple_error("History index should be in the range 1-15");
            goto cleanup;
        }

        parse_input(hist_array[index]);
    } else {
        *exit_status = 2;
        simple_error("Usage: log [<purge>] [<execute> <index>]");
        goto cleanup;
    }

    if (output_fd != NULL) {
        fprintf(output_fd, "%s\n", output);
    }

cleanup:
    free(argv);
    return output;
}

CC = gcc
CFLAGS = -Wall -Wextra
A_CFLAGS = $(CFLAGS) -g -fsanitize=address

SRCS = main.c prompt.c global.c exec.c system.c pastevents.c activities.c signals.c custom_commands.c input.c iman.c shrc.c
OBJS = $(SRCS:.c=.o)
A_OBJS = $(SRCS:.c=.address.o)

TARGET = a.out
A_TARGET = asan.out

.PHONY: all clean address

all: $(TARGET)

address: $(A_TARGET)

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@

$(A_TARGET): $(A_OBJS)
	$(CC) $(A_CFLAGS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

%.address.o: %.c
	$(CC) $(A_CFLAGS) -c $< -o $@

clean:
	rm -f $(TARGET) $(A_TARGET) $(OBJS) $(A_OBJS)

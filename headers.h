#ifndef HEADERS_H_
#define HEADERS_H_

// some usefull aliases
#include <arpa/inet.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <limits.h>
#include <math.h>
#include <netdb.h>
#include <pwd.h>
#include <regex.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#define MAX_ARGS 32
#define MAX_COMMANDS 64
#define IO_BUFFER 96000
#define MAX_SUBCOMMANDS 32
#define CMDLINE_PATH_FMT "/proc/%d/cmdline"
#define HOST_MAX 32
#define MAX_COMMANDS 64
#define PATH_MAX 4096
#define WHITESPACE " \n\t\r\v\f"

// coloring
#define ERROR "\033[1;31m"
#define RESET "\033[0m"
#define BOLD_GREEN "\033[1;32m"
#define BOLD_BLUE "\033[1;34m"
#define BOLD_CYAN "\033[1;36m"
#define BOLD "\033[1m"

// struct for commands
typedef enum {
    BG,
    FG,
} EXEC_TYPE;

typedef struct {
    char *data;
    EXEC_TYPE exec_type;
} command;

typedef unsigned int uint;

// structs for seek
struct match_struct {
    char *absolute_path;
    bool is_dir;
};

typedef struct match_struct *match;

// struct for processes
typedef enum {
    RUNNING,
    STOPPED,
    EXITED,
} PROC_STATUS;

typedef struct {
    char *name;
    pid_t pid;
} Process;

typedef struct {
    char *name;
    char *command;
} Alias;

typedef struct {
    char *name;
    char **commands;
    size_t l_commands;
} Function;

// from global.c which are useful
command *get_command(char *data, EXEC_TYPE exec_type);
bool starts_with(const char *str, const char *pre);
bool is_command(const char *str, const char *pre);
char *replace_string(const char *str, const char *match,
                     const char *replacement);
char *trim(char *str);
size_t parse_arguments(char *sub_command, char **arguments);
char *get_process_name_from_pid(pid_t pid);
void simple_error(char *string);
char *command_name(char *string);

// from main.c
size_t tokenize_input(char *input, command **command_array);
extern char homedir[PATH_MAX];
extern char curr_command[PATH_MAX];
void parse_input(const char *);

// from prompt.c
char *prompt();
char *getusername();
void update_prompt(char *name, size_t time);
void setup_prompt();

extern char pcwd[PATH_MAX];
extern char cwd[PATH_MAX];

// from exec.c
void execute_command(command *command_toexec);

// from system.c
char *system_commands(char *sub_command, char *input_buffer, int *exit_status,
                      bool is_first, bool is_last, EXEC_TYPE exec_type);
FILE *input_file(char *sub_command);
FILE *output_file(char *sub_command);

// from pasteventts.c
void store_input(const char *input);
char *pastevents(char *sub_command, int *exit_status);

// from signals.c
extern uint curr_fg;
extern char curr_fg_name[4096];
char *ping(char *sub_command, int *exit_status);
void ignore_signals();
void default_signals();
char *bg(char *sub_command, int *exit_status);
char *fg(char *sub_command, int *exit_status);
void killall_bg();

// from custom_commands.c
char *warp(char *sub_command, int *exit_status);
char *peek(char *sub_command, int *exit_status);
match get_match(const char *absolute_path, bool is_dir);
char *seek(char *sub_command, int *exit_status);

// for neonate and raw mode shit
char *take_input();
char *neonate(char *output, int *exit_stats);

// activities.c
extern char curr_fg_name[4096];
extern uint curr_fg;
extern Process bg_procs[MAX_COMMANDS];
extern size_t len_bg;

void append_process(char *name, pid_t pid);
char *activities(char *sub_command, int *exit_status);
char *proclore(char *sub_command, int *exit_status);

// from iman.c
char *iman(char *sub_command, int *exit_status);

// from shrc.c
extern Alias alias_list[MAX_COMMANDS];
extern size_t l_aliaslist;

extern Function func_list[MAX_COMMANDS];
extern size_t l_funclist;

void read_myshrc();
void free_aliases();
void free_functions();

#endif

#include "headers.h"

#define EXEC BOLD_GREEN
#define FOLDER BOLD_BLUE
#define SYMLINK BOLD_CYAN

char *
warp_chdir(const char *dir, int *exit_status) {
    if (chdir(dir) < 0) {
        simple_error(
            "Directory doesn't exist, or Missing permissions for task!");
        *exit_status = 3;
        chdir(cwd);
        return "";
    }

    strcpy(pcwd, cwd);
    getcwd(cwd, sizeof(cwd));
    return cwd;
}

char *
warp(char *sub_command, int *exit_status) {
    input_file(sub_command);
    FILE *output_fd = output_file(sub_command);
    char *output = (char *)calloc(IO_BUFFER, sizeof(char));
    char **argv = (char **)calloc(MAX_ARGS, sizeof(char *));
    char *dir = (char *)calloc(PATH_MAX, sizeof(char));
    size_t argc = 0;

    // remove the cd part
    char *arg = strtok(sub_command, WHITESPACE);

    while (arg != NULL) {
        argv[argc++] = arg;
        arg = strtok(NULL, WHITESPACE);
    }

    if (argc < 2) {
        strcpy(dir, homedir);
        strcat(output, warp_chdir(dir, exit_status));
        strcat(output, "\n");
    }

    for (size_t i = 1; i < argc; i++) {
        strcpy(dir, argv[i]);
        if (starts_with(dir, "~")) {
            char *coded = strdup(argv[i]);
            strcpy(dir, homedir);
            if (strcmp(coded, "~") != 0) {
                strcat(dir, &coded[2]);
            }
            free(coded);
        } else if (strcmp(dir, "-") == 0) {
            if (strlen(pcwd) == 0) {
                simple_error("hop: OLDPWD not set");
                *exit_status = 4;
                goto cleanup;
            }
            strcpy(dir, pcwd);
        }
        strcat(output, warp_chdir(dir, exit_status));
        strcat(output, "\n");
    }
    if (output_fd != NULL) {
        fprintf(output_fd, "%s\n", output);
    }

cleanup:
    free(argv);
    free(dir);
    return output;
}

// Comparison function for qsort
int
compare_dirent(const void *a, const void *b) {
    const struct dirent *da = *((const struct dirent **)a);
    const struct dirent *db = *((const struct dirent **)b);
    return strcmp(da->d_name, db->d_name);
}

// helpers for peek
void
get_perms(mode_t mode, char *output) {
    strcat(output, (S_ISDIR(mode)) ? "d" : "-");
    strcat(output, (mode & S_IRUSR) ? "r" : "-");
    strcat(output, (mode & S_IWUSR) ? "w" : "-");
    strcat(output, (mode & S_IXUSR) ? "x" : "-");
    strcat(output, (mode & S_IRGRP) ? "r" : "-");
    strcat(output, (mode & S_IWGRP) ? "w" : "-");
    strcat(output, (mode & S_IXGRP) ? "x" : "-");
    strcat(output, (mode & S_IROTH) ? "r" : "-");
    strcat(output, (mode & S_IWOTH) ? "w" : "-");
    strcat(output, (mode & S_IXOTH) ? "x" : "-");
}

bool
executable_perms(mode_t mode) {
    return (mode & S_IXOTH) || (mode & S_IXGRP) || (mode & S_IXUSR);
}

char *
group_name(gid_t gid) {
    struct group *group_info = getgrgid(gid);
    return group_info->gr_name;
}

char *
user_name(uid_t uid) {
    struct passwd *user_info = getpwuid(uid);
    return user_info->pw_name;
}

char *
gettime(struct timespec ts) {
    char *buffer = (char *)calloc(PATH_MAX, sizeof(char));
    struct tm time;
    localtime_r(&ts.tv_sec, &time);
    strftime(buffer, PATH_MAX - 1, "%b %d %H:%M", &time);
    return buffer;
}

// n^2 algo to remove duplicates
void
remove_duplicate_args(char *string, size_t len) {
    size_t j = 0;
    for (size_t i = 0; i < len; i++) {
        bool found = false;
        for (size_t k = 0; k < j; k++) {
            if (string[k] == string[i]) {
                found = 1;
                break;
            }
        }
        if (!found) {
            string[j++] = string[i];
        }
    }
    string[j] = '\0';
}

char *
peek(char *sub_command, int *exit_status) {
    input_file(sub_command);
    FILE *output_fd = output_file(sub_command);
    char *output = (char *)calloc(IO_BUFFER, sizeof(char));
    char **argv = (char **)calloc(MAX_ARGS, sizeof(char *));
    size_t argc = parse_arguments(sub_command, argv);

    bool l_flag = false;
    bool a_flag = false;
    bool dir_arg = false;
    char dir[PATH_MAX];

    bool no_arg = true;
    for (size_t i = 1; i < argc; i++) {
        if (starts_with(argv[i], "-") && strlen(argv[i]) > 1 &&
            (argv[i][1] == 'l' || argv[i][1] == 'a')) {
            remove_duplicate_args(argv[i], strlen(argv[i]));
            if ((strcmp(argv[i], "-la") == 0) | (strcmp(argv[i], "-al") == 0)) {
                l_flag = true, a_flag = true;
            } else if (strcmp(argv[i], "-l") == 0) {
                l_flag = true;
            } else if (strcmp(argv[i], "-a") == 0) {
                a_flag = true;
            }
        } else if (!dir_arg) {
            strcpy(dir, argv[i]);
            dir_arg = true;
            no_arg = false;
        } else {
            simple_error("Usage: reveal [-a|-l] <dir>");
        }
    }

    if (no_arg == true) {
        strcpy(dir, cwd);
    }

    // same as hop but change stuf
    if (starts_with(dir, "~")) {
        char *coded = strdup(dir);
        strcpy(dir, homedir);
        if (strcmp(coded, "~") != 0) {
            strcat(dir, &coded[2]);
        }
    }

    if (strcmp(dir, "-") == 0) {
        if (strlen(pcwd) == 0) {
            simple_error("reveal: previous directory not found");
            *exit_status = 4;
            free(argv);
            return output;
        }
        strcpy(dir, pcwd);
    }

    // finally open directory
    DIR *target = opendir(dir);
    if (target == NULL) {
        perror(ERROR "reveal" RESET);
        *exit_status = 3;
        free(argv);
        return output;
    }

    // read entities from a dir
    struct dirent **array =
        (struct dirent **)calloc(4096, sizeof(struct dirent *));
    size_t len_dir = 0;
    struct dirent *file = readdir(target);
    while (file != NULL) {
        array[len_dir++] = file;
        file = readdir(target);
    }

    // "Lexographic order"
    qsort(array, len_dir, sizeof(struct dirent *), compare_dirent);

    // file info declarations
    struct dirent *curr;
    char *file_name = (char *)calloc(PATH_MAX, sizeof(char));
    char *file_info = (char *)calloc(IO_BUFFER, sizeof(char));
    char *file_path = (char *)calloc(PATH_MAX, sizeof(char));
    char *perms = (char *)calloc(PATH_MAX, sizeof(char));
    int hlinks = 0;
    char *username = (char *)calloc(PATH_MAX, sizeof(char));
    char *groupname = (char *)calloc(PATH_MAX, sizeof(char));
    uint size;
    bool is_exec = false;
    char type;

    if (l_flag == true) {
        size_t total = 0;
        for (size_t i = 0; i < len_dir; i++) {
            curr = array[i];
            strcpy(file_name, curr->d_name);
            strcpy(file_path, dir);
            strcat(file_path, "/");
            strcat(file_path, file_name);
            struct stat info;
            type = curr->d_type;
            if (type != DT_LNK && stat(file_path, &info) < 0) {
                perror(ERROR "reveal" RESET);
                *exit_status = 4;
                goto cleanup;
            }
            if (type == DT_LNK && lstat(file_path, &info) < 0) {
                perror(ERROR "reveal" RESET);
                *exit_status = 4;
                goto cleanup;
            }
            total += info.st_blocks;
        }
        total /= 2;
        char total_str[30];
        memset(total_str, 0, sizeof(total));

        sprintf(total_str, "total %ld\n", total);
        strcat(output, total_str);
    }

    for (size_t i = 0; i < len_dir; i++) {
        curr = array[i];
        strcpy(file_name, curr->d_name);

        if (a_flag == false && starts_with(file_name, ".")) {
            continue;
        }

        strcpy(file_path, dir);
        strcat(file_path, "/");
        strcat(file_path, file_name);

        struct stat info;
        type = curr->d_type;
        if (type != DT_LNK && stat(file_path, &info) < 0) {
            perror(ERROR "reveal" RESET);
            *exit_status = 4;
            goto cleanup;
        }
        if (type == DT_LNK && lstat(file_path, &info) < 0) {
            perror(ERROR "reveal" RESET);
            *exit_status = 4;
            goto cleanup;
        }

        strcpy(perms, "");
        get_perms(info.st_mode, perms);

        hlinks = info.st_nlink;
        strcpy(username, user_name(info.st_uid));
        strcpy(groupname, group_name(info.st_gid));
        size = info.st_size;
        char *last_mod = gettime(info.st_mtim);
        is_exec = executable_perms(info.st_mode);

        if (l_flag == false) {
            if (type == DT_DIR) {
                sprintf(file_info, FOLDER "%s" RESET "/\n", file_name);
            } else if (type == DT_LNK) {
                sprintf(file_info, SYMLINK "%s" RESET "\n", file_name);
            } else if (is_exec == true) {
                sprintf(file_info, EXEC "%s" RESET "\n", file_name);
            } else {
                sprintf(file_info, "%s\n", file_name);
            }

            strcat(output, file_info);
            free(last_mod);
            continue;
        }

        if (type == DT_DIR)
            sprintf(file_info, "%s %d\t%s\t%s\t%d\t%s\t" FOLDER "%s" RESET "\n",
                    perms, hlinks, username, groupname, size, last_mod,
                    file_name);
        else if (type == DT_LNK)
            sprintf(file_info,
                    "%s %d\t%s\t%s\t%d\t%s\t" SYMLINK "%s" RESET "\n", perms,
                    hlinks, username, groupname, size, last_mod, file_name);
        else if (is_exec == true)
            sprintf(file_info, "%s %d\t%s\t%s\t%d\t%s\t" EXEC "%s" RESET "\n",
                    perms, hlinks, username, groupname, size, last_mod,
                    file_name);
        else
            sprintf(file_info, "%s %d\t%s\t%s\t%d\t%s\t%s\n", perms, hlinks,
                    username, groupname, size, last_mod, file_name);

        strcat(output, file_info);
        free(last_mod);
    }
    if (output_fd != NULL) {
        fprintf(output_fd, "%s", output);
        strcpy(output, "");
    }

    // Clean up allocated resources if needed
cleanup:
    free(file_info);
    free(file_name);
    free(file_path);
    free(perms);
    free(username);
    free(groupname);
    free(argv);
    free(array);
    closedir(target);
    return output;
}

void
seek_rec(const char *name, char *dir_path, size_t len_dir, match *matches,
         size_t *index) {
    DIR *target = opendir(dir_path);
    if (target == NULL) {
        simple_error("seek: Dir not found or missing permissions!");
        return;
    }

    struct dirent *file;
    while ((file = readdir(target))) {
        if (strcmp(file->d_name, ".") == 0 || strcmp(file->d_name, "..") == 0) {
            continue;
        }

        if (strstr(file->d_name, name)) {
            char new_absolute_path[PATH_MAX];
            strcpy(new_absolute_path, dir_path);
            strcat(new_absolute_path, "/");
            strcat(new_absolute_path, file->d_name);

            bool is_dir = false;
            if (file->d_type == DT_DIR) {
                is_dir = true;
            }
            if (*index < 409600) {
                matches[*index] = get_match(new_absolute_path, is_dir);
                *index += 1;
            } else {
                closedir(target);
                return;
            }
        }

        if (file->d_type == DT_DIR) {
            strcat(dir_path, "/");
            strcat(dir_path, file->d_name);
            size_t flength = strlen(file->d_name) + 1;
            seek_rec(name, dir_path, len_dir + flength, matches, index);
            dir_path[len_dir] = '\0';
        }
    }
    closedir(target);
}

char *
seek(char *sub_command, int *exit_status) {
    input_file(sub_command);
    FILE *output_fd = output_file(sub_command);
    char *output = (char *)calloc(IO_BUFFER, sizeof(char));
    char **argv = (char **)calloc(MAX_ARGS, sizeof(char *));
    size_t argc = parse_arguments(sub_command, argv);
    char usage[] = "Usage: seek [-f/-d] [-e] <name> [<dir>]";

    // recursive things
    bool e_flag = false;
    char d_flag = 0;
    bool no_dir = true;
    bool no_name = true;
    char *dir = (char *)calloc(PATH_MAX, sizeof(char));
    char name[PATH_MAX];
    memset(name, 0, PATH_MAX * sizeof(char));

    for (size_t i = 1; i < argc; i++) {
        if (starts_with(argv[i], "-") && strlen(argv[i]) > 1 &&
            (argv[i][1] == 'e' || argv[i][1] == 'f' || argv[i][1] == 'd')) {
            char *arg = strdup(argv[i]);
            size_t len = strlen(arg);
            remove_duplicate_args(arg, len);
            for (size_t index = 1; index < len; index++) {
                if (arg[index] == 'd') {
                    if (d_flag != 0) {
                        simple_error(usage);
                        *exit_status = 2;
                        goto cleanup;
                    }
                    d_flag = 1;
                } else if (arg[index] == 'f') {
                    if (d_flag != 0) {
                        simple_error(usage);
                        *exit_status = 2;
                        goto cleanup;
                    }
                    d_flag = 2;
                } else if (arg[index] == 'e') {
                    e_flag = true;
                }
            }
            free(arg);
        } else {
            if (no_name == false) {
                if (no_dir == false) {
                    simple_error(usage);
                    goto cleanup;
                } else {
                    if (!starts_with(argv[i], "~") &&
                        !starts_with(argv[i], "-") &&
                        !starts_with(argv[i], "/")) {
                        strcpy(dir, cwd);
                        strcat(dir, "/");
                        strcat(dir, argv[i]);
                    } else {
                        strcpy(dir, argv[i]);
                    }
                    no_dir = false;
                }
            } else {
                strcpy(name, argv[i]);
                no_name = false;
            }
        }
    }

    if (no_name == true) {
        simple_error(usage);
        *exit_status = 2;
        goto cleanup;
    }

    if (no_dir == true) {
        strcpy(dir, cwd);
    }

    // same as hop but change stuff, replace ~
    if (starts_with(dir, "~")) {
        char *coded = strdup(dir);
        strcpy(dir, homedir);
        if (strcmp(coded, "~") != 0) {
            strcat(dir, &coded[2]);
        }
        free(coded);
    }

    match matches[409600];
    size_t index = 0;

    if (starts_with(dir, "-")) {
        if (strlen(pcwd) == 0) {
            simple_error("seek: previous directory not found");
            *exit_status = 4;
            goto cleanup;
        }
        strcpy(dir, pcwd);
    }

    seek_rec(name, dir, strlen(dir), matches, &index);

    // after matching
    for (size_t i = 0; i < index; i++) {
        if (matches[i]->is_dir == true) {
            if (d_flag == 2)
                continue;
            if (e_flag == false) {
                strcat(output, FOLDER);
                char *better_string =
                    replace_string(matches[i]->absolute_path, dir, ".");
                if (strlen(output) > IO_BUFFER - 100) {
                    strcat(output, RESET);
                    strcat(output, "\n");
                    free(better_string);
                    break;
                } else {
                    strcat(output, better_string);
                }
                free(better_string);
            } else {
                char sub_command[4096];
                memset(sub_command, 0, 4096);
                strcat(sub_command, "hop ");
                strcat(sub_command, matches[i]->absolute_path);
                strcpy(output, warp(sub_command, exit_status));
                break;
            }
        } else {
            if (d_flag == 1)
                continue;
            if (e_flag == false) {
                strcat(output, EXEC);
                char *better_string =
                    replace_string(matches[i]->absolute_path, dir, ".");
                if (strlen(output) > IO_BUFFER - 100) {
                    strcat(output, RESET);
                    strcat(output, "\n");
                    free(better_string);
                    break;
                } else {
                    strcat(output, better_string);
                }
                free(better_string);
            } else {
                FILE *catter = fopen(matches[i]->absolute_path, "r");
                if (catter == NULL) {
                    simple_error("seek: Missing permissions to read file!");
                    *exit_status = 3;
                    goto cleanup;
                }
                fread(output, 1, IO_BUFFER - 1, catter);
                break;
            }
        }
        strcat(output, RESET);
        strcat(output, "\n");
    }

    if (output_fd != NULL) {
        fprintf(output_fd, "%s", output);
        strcpy(output, "");
    }

cleanup:
    for (size_t i = 0; i < index; i++) {
        free(matches[i]->absolute_path);
        free(matches[i]);
    }
    free(dir);
    free(argv);
    return output;
}

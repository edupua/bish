#include "headers.h"

void
default_signals() {
    signal(SIGINT, SIG_DFL);
    signal(SIGTSTP, SIG_DFL);
}

void
ignore_signals() {
    signal(SIGINT, SIG_IGN);
    signal(SIGTSTP, SIG_IGN);
}

// ping command
char *
ping(char *sub_command, int *exit_status) {
    char *output = (char *)calloc(IO_BUFFER, sizeof(char));
    char **argv = (char **)calloc(MAX_ARGS, sizeof(char *));
    size_t argc = parse_arguments(sub_command, argv);

    if (argc != 3) {
        simple_error("Usage: ping <pid> <signal_number>");
        *exit_status = 2;
        return output;
    }

    pid_t pid = atoi(argv[1]);
    uint signal = atoi(argv[2]) % 32;

    if (kill(pid, signal) < 0) {
        if (errno == EINVAL)
            simple_error("No such process found.");
        else
            perror(ERROR "ping" RESET);
        *exit_status = 3;
        return output;
    }

    fprintf(stderr, "Sent signal %d to process with pid %d\n", signal, pid);

    free(argv);
    return output;
}

// fg bg commands
char *
fg(char *sub_command, int *exit_status) {
    char *output = (char *)calloc(IO_BUFFER, sizeof(char));
    char **argv = (char **)calloc(MAX_ARGS, sizeof(char *));
    size_t argc = parse_arguments(sub_command, argv);

    if (argc != 2) {
        *exit_status = 2;
        return output;
        goto cleanup;
    }
    pid_t pid = atoi(argv[1]);

    // check if process is running or is exited
    pid_t return_pid = waitpid(pid, NULL, WNOHANG);
    if (return_pid == -1) {
        perror(ERROR "fg" RESET);
        *exit_status = 5;
        return output;
    } else if (return_pid == pid) {
        fprintf(stderr,
                ERROR "fg: The process (%d) is not running/stopped \n" RESET,
                pid);
        *exit_status = 4;
        goto cleanup;
    }

    // give the process stdin access
    signal(SIGTTIN, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);
    default_signals();

    // bring any stopped process running;
    if (kill(-pid, SIGCONT) < 0) {
        if (errno == EINVAL)
            simple_error("No such process found.");
        else
            perror(ERROR "fg continue:" RESET);
        *exit_status = 3;
        goto cleanup;
    }

    if (tcsetpgrp(STDIN_FILENO, getpgid(pid)) == -1) {
        perror(ERROR "Failed to set foreground process group\n" RESET);
        goto cleanup;
    }

    curr_fg = pid;
    strcpy(curr_fg_name, argv[0]);

    fprintf(stderr, "Send pid (%d) to foreground\n", pid);

    int status = 0;
    waitpid(pid, &status, WUNTRACED);
    // see how child exited
    if (WIFSIGNALED(status)) {
        fprintf(stderr, ERROR "Process (%d) terminated by signal: %d\n" RESET,
                pid, WTERMSIG(status));
    } else if (WIFSTOPPED(status)) {
        append_process(curr_fg_name, pid);
        fprintf(stderr, ERROR "Process (%d) stopped by signal: %d\n" RESET, pid,
                WSTOPSIG(status));
    }

    // give back controlling terminal to shell
    tcsetpgrp(STDIN_FILENO, getpid());

    signal(SIGTTIN, SIG_DFL);
    signal(SIGTTOU, SIG_DFL);
    ignore_signals();

cleanup:
    free(argv);
    return output;
}

char *
bg(char *sub_command, int *exit_status) {
    char *output = (char *)calloc(IO_BUFFER, sizeof(char));

    char **argv = (char **)calloc(MAX_ARGS, sizeof(char *));
    size_t argc = parse_arguments(sub_command, argv);

    if (argc != 2) {
        simple_error("Usage: bg <pid>");
        *exit_status = 2;
        return output;
    }
    pid_t pid = atoi(argv[1]);

    if (kill(-pid, SIGCONT) < 0) {
        if (errno == EINVAL)
            simple_error("No such process found.");
        else
            perror(ERROR "bg" RESET);
        *exit_status = 3;
        return output;
    }
    fprintf(stderr, "Pid (%d) is resumed in the background\n", pid);

    // update list stored in this program
    free(argv);

    return output;
}

#include "headers.h"

struct termios orig_termios;

void
disableRawMode() {
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_termios) == -1) {
        perror(ERROR "tcsetattr" RESET);
        exit(1);
    }
}

void
enableRawMode() {
    if (tcgetattr(STDIN_FILENO, &orig_termios) == -1) {
        perror(ERROR "tcgetattr" RESET);
        exit(1);
    }
    atexit(disableRawMode);
    struct termios raw = orig_termios;
    raw.c_lflag &= ~(ICANON | ECHO);
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) == -1) {
        perror(ERROR "tcsetattr" RESET);
        exit(1);
    }
}

char *
take_input() {
    char *input = (char *)calloc(4096, sizeof(char));
    size_t index = 0;
    int c = 0;

    setbuf(stdout, NULL);
    enableRawMode();

    char *prompt_str = prompt();
    fprintf(stdout, "%s", prompt_str);
    free(prompt_str);

    // initializee input

    while (read(STDIN_FILENO, &c, 1) == 1) {
        if (iscntrl(c)) {
            if (c == 10) {
                break; // new line, our command is done when this is read
            } else if (c == 27) { // escape; could be followed by a code
                char buf[3];
                buf[2] = 0;
                if (read(STDIN_FILENO, buf, 2) == 2) { // length of escape
                    printf("\rarrow key: %s", buf);
                }
            } else if (c == 127) { // backspace
                if (index == 0)
                    continue;
                if (input[index - 1] == 9) {
                    for (int i = 0; i < 7; i++)
                        printf("\b");
                }
                input[--index] = '\0';
                printf("\b \b");
            } else if (c == 9) { // TAB character
                input[index++] = c;
                for (int i = 0; i < 8;
                     i++) { // fake a tab by pushing forward 8 spaces
                    printf(" ");
                }
            } else if (c == 4 || c == EOF) {
                killall_bg();
                exit(EXIT_SUCCESS);
            }
        } else {
            input[index++] = c;
            printf("%c", c);
        }
    }
    disableRawMode();
    printf("\n");

    return input;
}

pid_t
get_latest_pid() {

    FILE *loadavg = fopen("/proc/loadavg", "r");
    if (loadavg == NULL) {
        simple_error("Error opening file: /proc/loadavg");
        exit(EXIT_FAILURE);
    }

    char buffer[IO_BUFFER];
    fread(buffer, 1, IO_BUFFER - 1, loadavg);
    fclose(loadavg);

    char *token = strtok(buffer, " ");
    pid_t pid;
    while (token != NULL) {
        pid = atoi(token);
        token = strtok(NULL, " ");
    }

    return pid;
}

char *
neonate(char *sub_command, int *exit_status) {
    input_file(sub_command);
    output_file(sub_command);
    char *output = (char *)calloc(IO_BUFFER, sizeof(char));
    char **argv = (char **)calloc(MAX_ARGS, sizeof(char *));
    size_t argc = parse_arguments(sub_command, argv);

    if (argc != 3 || strcmp(argv[1], "-n")) {
        simple_error("Usage: neonate -n <time>");
        *exit_status = 2;
        free(argv);
        return output;
    }

    int c = 0;
    setbuf(stdout, NULL);
    enableRawMode();
    simple_error("Press 'x' to end the command.");

    pid_t pid;

    if ((pid = fork()) < 0) {
        perror(ERROR "fork:" RESET);
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        while (1) {
            sleep(1);
            printf("%d\n", get_latest_pid());
        }
        exit(EXIT_SUCCESS);
    }

    while (read(STDIN_FILENO, &c, 1) == 1) {
        if (c == 'x') {
            kill(pid, SIGKILL);
            break;
        }
    }
    disableRawMode();
    free(argv);
    return output;
}

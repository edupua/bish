#include "headers.h"

// alias management
Alias alias_list[MAX_COMMANDS];
size_t l_aliaslist = 0;

Function func_list[MAX_COMMANDS];
size_t l_funclist = 0;

void
append_alias(char *input) {
    // name = command
    strtok_r(input, WHITESPACE, &input);
    char *name = strtok_r(input, "=", &input);
    char *commented_out = strtok(input, "#");

    alias_list[l_aliaslist].name = strdup(trim(name));
    alias_list[l_aliaslist].command = strdup(commented_out);
    l_aliaslist++;
}

void
free_aliases() {
    for (size_t i = 0; i < l_aliaslist; i++) {
        free(alias_list[i].name);
        free(alias_list[i].command);
    }
}

// function management
void
read_function(char lines[][4096], size_t l_lines, const char *name) {
    func_list[l_funclist].name = strdup(name);
    func_list[l_funclist].commands =
        (char **)calloc(MAX_COMMANDS, sizeof(char *));
    func_list[l_funclist].l_commands = l_lines;

    for (size_t i = 0; i < l_lines; i++) {
        char *better_line = trim(strtok(lines[i], "#"));
        func_list[l_funclist].commands[i] = strdup(better_line);
    }
    l_funclist++;
    return;
}

void
free_functions() {
    for (size_t i = 0; i < l_funclist; i++) {
        size_t len = func_list[i].l_commands;
        for (size_t j = 0; j < len; j++) {
            free(func_list[i].commands[j]);
        }
        free(func_list[i].name);
    }
}

void
read_myshrc() {
    char input[4096];
    FILE *myshrc = fopen(".myshrc", "r");
    if (myshrc == NULL) {
        if (errno == ENOENT) {
            printf("Note: .myshrc file not found. Using default settings.\n");
            myshrc = fopen(".myshrc", "w");
            if (myshrc != NULL) {
                fprintf(myshrc, "# Default .myshrc file\n");
                fclose(myshrc);
            }
            return;
        } else {
            perror("Error opening .myshrc");
            return;
        }
    }

    int inside_func = 0;
    char buffer[MAX_COMMANDS][4096];
    size_t l_lines = 0;
    char *name;

    while (fgets(input, 4096, myshrc) != NULL) {
        if (inside_func == 0) {
            if (is_command(input, "alias")) {
                append_alias(input);
            }

            if (is_command(input, "func")) {
                char *temp1 = &input[4];
                char *temp2 = strstr(temp1, "()");
                *temp2 = '\0';
                name = strdup(trim(temp1));
                inside_func = 1;
            }
        } else if (inside_func == 1) {
            if (is_command(input, "{")) {
                inside_func = 2;
            } else {
                simple_error("syntax error in .myshrc: Expected '{' after "
                             "function definition");
                return;
            }
        } else if (inside_func == 2) {
            if (is_command(input, "}")) {
                read_function(buffer, l_lines, name);
                free(name);
                l_lines = 0;
                inside_func = 0;
            } else if (strlen(input) > 0) {
                strcpy(buffer[l_lines], input);
                l_lines++;
            }
        }
    }
    if (inside_func != 0) {
        simple_error("syntax error in .myshrc: Expected '}' after "
                     "got EOF");
    }

    return;
}

#include "headers.h"

Process bg_procs[MAX_COMMANDS];
size_t len_bg = 0;

uint curr_fg = 0;
char curr_fg_name[4096];

void
killall_bg() {
    for (size_t i = 0; i < len_bg; i++) {
        kill(bg_procs[i].pid, SIGKILL);
    }
}

void
append_process(char *name, pid_t pid) {
    // avoid duplicates
    for (size_t i = 0; i < len_bg; i++)
        if (bg_procs[i].pid == pid)
            return;

    Process new_proc;
    char *proc_name = strdup(name);
    new_proc.name = proc_name;
    new_proc.pid = pid;

    // append the new process
    bg_procs[len_bg++] = new_proc;
}

char *
activities(char *sub_command, int *exit_status) {
    input_file(sub_command);
    FILE *output_fd = output_file(sub_command);
    char *output = (char *)calloc(IO_BUFFER, sizeof(char));
    char **argv = (char **)calloc(MAX_ARGS, sizeof(char *));
    size_t argc = parse_arguments(sub_command, argv);

    if (argc > 1) {
        simple_error("activities takes no argument");
        *exit_status = 2;
        free(argv);
        return output;
    }

    char curr[4096];
    char buffer[IO_BUFFER];
    for (size_t i = 0; i < len_bg; i++) {
        Process proc = bg_procs[i];
        PROC_STATUS ps = EXITED;

        char proc_path[PATH_MAX];
        memset(proc_path, 0, PATH_MAX * sizeof(char));
        sprintf(proc_path, "/proc/%d/stat", proc.pid);

        memset(buffer, 0, IO_BUFFER * sizeof(char));
        FILE *stat = fopen(proc_path, "r");
        if (!stat) {
            continue;
        }

        fread(buffer, sizeof(char), sizeof(buffer) - 1, stat);
        fclose(stat);

        strtok(buffer, WHITESPACE);
        strtok(NULL, WHITESPACE);
        char *token = strtok(NULL, WHITESPACE);
        if (strcmp(token, "T") == 0) {
            ps = STOPPED;
        } else if (strcmp(token, "S") == 0) {
            ps = RUNNING;
        }

        switch (ps) {
        case RUNNING:
            sprintf(curr, "%d : %s - Running\n", proc.pid, proc.name);
            break;
        case STOPPED:
            sprintf(curr, "%d : %s - Stopped\n", proc.pid, proc.name);
            break;
        case EXITED:
            strcpy(curr, "");
        }
        strcat(output, curr);
    }

    if (output_fd != NULL) {
        fprintf(output_fd, "%s\n", output);
    }

    free(argv);
    return output;
}

char *
proclore(char *sub_command, int *exit_status) {
    input_file(sub_command);
    FILE *output_fd = output_file(sub_command);
    char *output = (char *)calloc(IO_BUFFER, sizeof(char));
    char **argv = (char **)calloc(MAX_ARGS, sizeof(char *));
    size_t argc = parse_arguments(sub_command, argv);

    char pid[16];
    pid_t id;
    char proc_path[PATH_MAX];
    char exec_path[PATH_MAX - 10];
    char executable[PATH_MAX];
    char buffer[IO_BUFFER];
    char stat_list[PATH_MAX][24];

    memset(executable, 0, PATH_MAX * sizeof(char));
    memset(proc_path, 0, PATH_MAX * sizeof(char));
    memset(exec_path, 0, (PATH_MAX - 10) * sizeof(char));
    memset(buffer, 0, IO_BUFFER * sizeof(char));
    for (size_t i = 0; i < 24; i++) {
        memset(stat_list[i], 0, PATH_MAX * sizeof(char));
    }

    if (argc == 1) {
        id = getpid();
        sprintf(pid, "%d", id);
    } else if (argc == 2) {
        strcpy(pid, argv[1]);
        id = atoi(pid);
    } else {
        simple_error("Usage: proclore [<pid>]");
        *exit_status = 2;
        free(argv);
        return output;
    }

    sprintf(proc_path, "/proc/%s/stat", pid);
    sprintf(exec_path, "/proc/%s/exe", pid);

    // read symlink value
    if (readlink(exec_path, executable, PATH_MAX - 1) < 0) {
        simple_error("Process not found or exited");
        *exit_status = 4;
        free(argv);
        return output;
    }

    FILE *stat = fopen(proc_path, "r");

    if (stat == NULL) {
        simple_error("Given process doesn't exist");
        *exit_status = 3;
        free(argv);
        return output;
    }

    fread(buffer, sizeof(char), sizeof(buffer) - 1, stat);
    fclose(stat);

    // read the stat file to get rest of the data
    char *token = strtok(buffer, WHITESPACE);
    for (size_t i = 0; i < 24 && token != NULL; i++) {
        strcpy(stat_list[i], token);
        token = strtok(NULL, WHITESPACE);
    }

    char status[5];
    strcpy(status, stat_list[2]);

    if (atoi(stat_list[4]) == tcgetpgrp(STDIN_FILENO)) {
        strcat(status, "+");
    }

    // now form the output list
    char output_chunk[IO_BUFFER];
    sprintf(output_chunk, "pid : %d\n", id);
    strcat(output, output_chunk);
    sprintf(output_chunk, "process Status : %s\n", status);
    strcat(output, output_chunk);
    sprintf(output_chunk, "Process Group : %s\n", stat_list[4]);
    strcat(output, output_chunk);
    sprintf(output_chunk, "Virtual Memory : %s\n", stat_list[22]);
    strcat(output, output_chunk);
    sprintf(output_chunk, "executable Path : %s\n", executable);
    strcat(output, output_chunk);

    if (output_fd != NULL) {
        fprintf(output_fd, "%s\n", output);
    }
    free(argv);
    return output;
}

/* a file for useful functions, to compensate shittiness of C
 * It contains structure implementation of repeated string operations
 * It also contains general stuff which needs to be shared to all files  */

#include "headers.h"

// makes a new instance of command object
command *
get_command(char *data, EXEC_TYPE exec_type) {
    command *new_command = (command *)calloc(1, sizeof(command));
    new_command->data = data;
    new_command->exec_type = exec_type;
    return new_command;
}

// returns if the string given starts with the prefix
bool
starts_with(const char *str, const char *pre) {
    return strncmp(pre, str, strlen(pre)) == 0;
}

bool
is_command(const char *str, const char *pre) {
    char *string = strdup(str);
    char *token = strtok(string, WHITESPACE);
    bool ret = false;
    if (token) {
        ret = strcmp(token, pre) == 0;
    }
    free(string);
    return ret;
}

/* used to trim whitespace both from behind and foreward
 * source: https://stackoverflow.com/a/122974 */
char *
trim(char *str) {
    size_t len = 0;
    char *frontp = str;
    char *endp = NULL;

    if (str == NULL) {
        return NULL;
    }
    if (str[0] == '\0') {
        return str;
    }

    len = strlen(str);
    endp = str + len;

    /* Move the front and back pointers to address the first non-whitespace
     * characters from each end.
     */
    while (isspace((unsigned char)*frontp)) {
        ++frontp;
    }
    if (endp != frontp) {
        while (isspace((unsigned char)*(--endp)) && endp != frontp) {
        }
    }

    if (frontp != str && endp == frontp)
        *str = '\0';
    else if (str + len - 1 != endp)
        *(endp + 1) = '\0';

    /* Shift the string so that it starts at str so that if it's dynamically
     * allocated, we can still free it on the returned pointer.  Note the reuse
     * of endp to mean the front of the string buffer now.
     */
    endp = str;
    if (frontp != str) {
        while (*frontp) {
            *endp++ = *frontp++;
        }
        *endp = '\0';
    }
    return str;
}

// To be used with starts_with(), replaces a string with other when a matcher is
// matches the first few chars of string and replaces with the replacement
// string
char *
replace_string(const char *str, const char *match, const char *replacement) {
    // we are sure that match exists in str so we pass an offset
    int index = strlen(match);
    char *new_string = (char *)calloc(1, PATH_MAX);
    new_string = strcat(new_string, replacement);
    new_string = strcat(new_string, &str[index]);
    return new_string;
}

size_t
parse_arguments(char *sub_command, char **arguments) {
    size_t n = 0;
    char *ptr = sub_command;
    char matcher = '\0';

    while (*ptr && n < MAX_ARGS) {
        while (*ptr && isspace((unsigned char)*ptr)) {
            ptr++;
        }
        if (!*ptr) {
            break;
        }
        if (*ptr == '"' || *ptr == '\'' || *ptr == '`') {
            matcher = *ptr;
            ptr++;
            arguments[n++] = ptr;
            while (*ptr && *ptr != matcher) {
                ptr++;
            }
            if (*ptr == matcher) {
                *ptr = '\0';
                ptr++;
            }
        } else {
            arguments[n++] = ptr;
            while (*ptr && !isspace((unsigned char)*ptr)) {
                ptr++;
            }
            if (*ptr) {
                *ptr = '\0';
                ptr++;
            }
        }
    }
    return n;
}

// for seek.h
match
get_match(const char *absolute_path, bool is_dir) {
    match new_match = (match)calloc(1, sizeof(struct match_struct));
    new_match->absolute_path = (char *)calloc(PATH_MAX, sizeof(char));
    strcpy(new_match->absolute_path, absolute_path);
    new_match->is_dir = is_dir;
    return new_match;
}

// for better error generation
void
simple_error(char *string) {
    fprintf(stderr, ERROR "%s\n" RESET, string);
    return;
}

// extract command name of of command
char *
command_name(char *string) {
    return strtok(string, WHITESPACE);
}

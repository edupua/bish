#include "headers.h"

char pcwd[PATH_MAX];
char cwd[PATH_MAX];

typedef struct {
    char *name;
    size_t time;
} long_commands;

size_t l_longlist;
long_commands long_list[MAX_COMMANDS];

void
setup_prompt() {
    for (size_t i = 0; i < l_longlist; i++)
        free(long_list[i].name);
    l_longlist = 0;
}

void
update_prompt(char *name, size_t time) {
    long_list[l_longlist].name = (char *)calloc(PATH_MAX, sizeof(name));
    strcpy(long_list[l_longlist].name, name);
    long_list[l_longlist].time = time;
    l_longlist++;
}

// a function to get the current username using <pwd.h> stdL
char *
getusername() {
    uid_t uid = getuid();

    struct passwd *pw = getpwuid(uid);
    if (pw == NULL) {
        perror(ERROR "getpwuid" RESET);
        exit(EXIT_FAILURE);
    }
    return pw->pw_name;
}

char *
prompt() {
    // get the uesrname and the hostname
    char *hostname = (char *)calloc(PATH_MAX, sizeof(char));
    gethostname(hostname, HOST_MAX);
    char *username = getusername();
    char *output = (char *)calloc(PATH_MAX, sizeof(char));
    char *ccwd;
    char first[10000];
    char second[10000];
    char third[10000];
    char *prefix = (char *)calloc(PATH_MAX, sizeof(char));

    // find cwd
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        strcpy(prefix, homedir);

        // replace home with tilde
        if (starts_with(cwd, prefix))
            ccwd = replace_string(cwd, prefix, "~");
        else {
            ccwd = strdup(cwd);
        }
    } else {
        perror(ERROR "getcwd failure: unable to fetch cwd" RESET);
    }

    sprintf(first, "<" BOLD_GREEN "%s@%s" BOLD ":" BOLD_BLUE "%s" RESET,
            username, hostname, ccwd);
    for (size_t i = 0; i < l_longlist; i++) {
        sprintf(second, " %s : %lds", long_list[i].name, long_list[i].time);
        strcat(first, second);
    }
    sprintf(third, "> ");

    strcpy(output, first);
    strcat(output, third);

    free(ccwd);
    free(prefix);
    free(hostname);
    return output;
}

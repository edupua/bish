# Description
This is a minimal shell in C, which is part of the course [OSN (Operating Systems and Networking)](https://karthikv1392.github.io/cs3301_osn/home/).

The project specification link is: [here](https://karthikv1392.github.io/cs3301_osn/mini-projects/mp1).
NOTE: i wrote this shell a while ago, from my batch friend's docs. So command
names and all will be different, but i changed them accordingly but function
names and all i didn't change yet.

## Assumptions 

- Stderr gives red only for fprintf errors, perror only gives the customly added
  part red, the auto generated one is normal. 
- I am using ascii sort whenever i have to sort lexographically, many juniors in
  the doubt doc seem to be confused but i did what's easier.

## Features

- The `log execute` command also adds itself to the prompt in the next command
  if its takes longer than 2s.
- There is color for symlinks. 

#include "headers.h"
#include <bits/time.h>

char homedir[PATH_MAX];
char curr_command[PATH_MAX];

void
store_homedir() {
    getcwd(homedir, sizeof(homedir));
}

size_t
tokenize_input(char *input, command **command_array) {
    // get the foreground processes

    char *token_array[MAX_COMMANDS];
    size_t ta_index = 0;

    char *token = strtok(input, ";");
    while (token != NULL) {
        // parse previous token
        token_array[ta_index++] = strdup(token);
        token = strtok(NULL, ";\n");
    }

    size_t ca_index = 0;
    // populate the command array
    for (size_t i = 0; i < ta_index; i++) {
        if (strchr(token_array[i], '&') == NULL) {
            // get foregournd
            char *trimmed_token = trim(token_array[i]);
            if (strcmp(trimmed_token, ""))
                command_array[ca_index++] = get_command(trimmed_token, FG);
        } else {
            // get background commands
            char *bg_input = token_array[i];
            char *bg_token = strtok(bg_input, "&");
            uint count = 0;
            while (bg_token != NULL) {
                // put this valid bg token in command_array
                char *trimmed_token = strdup(trim(bg_token));
                if (strcmp(trimmed_token, "") != 0) {
                    count += 1;
                    command_array[ca_index++] = get_command(trimmed_token, BG);
                }
                bg_token = strtok(NULL, "&");
            }
            // fix: the last bg_token was actually foreground, except if there
            // was one token
            if (count > 1)
                command_array[ca_index - 1]->exec_type = FG;
            free(bg_input);
        }
    }
    return ca_index;
}

void
wait_for_background(int sig) {
    // Check if any background process has exited
    int status;
    (void)sig;
    pid_t exited_pid;
    while ((exited_pid = waitpid(-1, &status, WNOHANG)) > 0) {
        // iterate through the proclist to match pid to get the name and stuff
        int index = 0;
        for (index = len_bg - 1; index >= 0; index--)
            if (bg_procs[index].pid == exited_pid)
                break;
        if (index < 0)
            break;
        char *process_name = bg_procs[index].name;
        if (status == 0)
            fprintf(stderr, "'%s' (%d) has exited normally\n", process_name,
                    exited_pid);
        else
            fprintf(stderr,
                    ERROR "'%s' (%d) has exited with status: %d\n" RESET,
                    process_name, exited_pid, status % 256);
    }
}

void
parse_input(const char *input) {
    char *string = strdup(input);
    command **t_commands = (command **)calloc(MAX_COMMANDS, sizeof(command *));
    size_t time_taken;
    struct timespec start_time, end_time;
    size_t tc_length = 0;

    // wait for any background processes to stop
    struct sigaction sa;
    sa.sa_handler = wait_for_background;
    sa.sa_flags = SA_RESTART;
    sigaction(SIGCHLD, &sa, NULL);

    setup_prompt();
    tc_length = tokenize_input(string, t_commands);

    for (size_t i = 0; i < tc_length; i++) {
        clock_gettime(CLOCK_REALTIME, &start_time);
        execute_command(t_commands[i]);
        clock_gettime(CLOCK_REALTIME, &end_time);

        time_taken = end_time.tv_sec - start_time.tv_sec;
        if (time_taken > 1) {
            update_prompt(command_name(t_commands[i]->data), time_taken);
        }
    }

    for (size_t i = 0; i < tc_length; i++) {
        free(t_commands[i]->data);
        free(t_commands[i]);
    }
    free(t_commands);
    free(string);
    return;
}

int
main() {

    // Keep accepting commands
    store_homedir();
    ignore_signals();
    read_myshrc();

    while (1) {
        char *input = take_input();
        parse_input(input);
        store_input(input);
        free(input);
    }

    free_aliases();
    free_functions();
}

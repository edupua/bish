#include "headers.h"

#define IO_BUFFER 96000
#define MAX_SUBCOMMANDS 32

void send_subcommand_forexec(char *, char *, char *, int *, bool, bool,
                             EXEC_TYPE);

void
execute_function(char *sub_command, char *input_buffer, char *output_buffer,
                 int *exit_status, bool is_first, bool is_last,
                 EXEC_TYPE exec_type, size_t index) {
    char *argv[MAX_ARGS];
    char *better_subcommand = (char *)calloc(4096, sizeof(char));
    size_t argc = parse_arguments(sub_command, argv);
    size_t l_commands = func_list[index].l_commands;

    for (size_t i = 0; i < l_commands; i++) {
        // for each command find and replace integers
        memset(better_subcommand, 0, 4096);
        char *temp = strdup(func_list[index].commands[i]);
        char matcher[PATH_MAX];
        char *argv2[MAX_ARGS];
        bool found;
        size_t argc2 = parse_arguments(temp, argv2);

        for (size_t k = 0; k < argc2; k++) {
            found = false;
            for (size_t j = 1; j < MAX_ARGS; j++) {
                sprintf(matcher, "$%ld", j);
                if (strcmp(argv2[k], matcher) == 0) {
                    if (argc > MAX_ARGS) {
                        fprintf(stderr, ERROR "Usage: %s <%ld args>" RESET,
                                argv[0], j);
                        return;
                    }
                    strcat(better_subcommand, argv[j]);
                    found = true;
                }
            }
            if (!found) {
                strcat(better_subcommand, argv2[k]);
            }
            strcat(better_subcommand, " ");
        }
        free(temp);

        send_subcommand_forexec(trim(better_subcommand), input_buffer,
                                output_buffer, exit_status, is_first, is_last,
                                exec_type);
    }
    free(better_subcommand);
}

void
send_subcommand_forexec(char *sub_command, char *input_buffer,
                        char *output_buffer, int *exit_status, bool is_first,
                        bool is_last, EXEC_TYPE exec_type) {

    char *output = "";
    if (strcmp(sub_command, "exit") == 0) {
        exit(EXIT_SUCCESS);
    } else if (is_command(sub_command, "hop")) {
        output = warp(sub_command, exit_status);
    } else if (is_command(sub_command, "reveal")) {
        output = peek(sub_command, exit_status);
    } else if (is_command(sub_command, "log")) {
        output = pastevents(sub_command, exit_status);
    } else if (is_command(sub_command, "seek")) {
        output = seek(sub_command, exit_status);
    } else if (is_command(sub_command, "proclore")) {
        output = proclore(sub_command, exit_status);
    } else if (is_command(sub_command, "activities")) {
        output = activities(sub_command, exit_status);
    } else if (is_command(sub_command, "ping")) {
        output = ping(sub_command, exit_status);
    } else if (is_command(sub_command, "fg")) {
        output = fg(sub_command, exit_status);
    } else if (is_command(sub_command, "bg")) {
        output = bg(sub_command, exit_status);
    } else if (is_command(sub_command, "neonate")) {
        output = neonate(sub_command, exit_status);
    } else if (is_command(sub_command, "iMan")) {
        output = iman(sub_command, exit_status);
    } else {
        // try aliases
        for (size_t i = 0; i < l_aliaslist; i++) {
            if (strcmp(sub_command, alias_list[i].name) == 0) {
                send_subcommand_forexec(alias_list[i].command, input_buffer,
                                        output_buffer, exit_status, is_first,
                                        is_last, exec_type);
                return;
            }
        }

        // try functions
        for (size_t i = 0; i < l_funclist; i++) {
            if (is_command(sub_command, func_list[i].name)) {
                execute_function(sub_command, input_buffer, output_buffer,
                                 exit_status, is_first, is_last, exec_type, i);
                return;
            }
        }

        output = system_commands(sub_command, input_buffer, exit_status,
                                 is_first, is_last, exec_type);
    }

    strcpy(output_buffer, output);
    free(output);
}

void
execute_command(command *command_toexec) {

    // handle pipes by tokenizing
    char **pipe_list = (char **)malloc(MAX_SUBCOMMANDS * sizeof(char *));
    size_t pl_index = 0;

    // save a copy of the original command
    char command_data[4096];
    strcpy(command_data, command_toexec->data);

    // handle case: starts or ends with a pipe '|'
    if (is_command(command_data, "|") ||
        command_data[strlen(command_data) - 1] == '|') {
        simple_error("syntax error: Invalid use of pipe");
        goto cleanup;
    }

    char *sub_command = strtok(command_data, "|");
    while (sub_command != NULL) {
        char *trimmed_subcommand = trim(sub_command);
        if (strlen(trimmed_subcommand) == 0) {
            simple_error("syntax error: Invalid use of pipe");
            goto cleanup;
        }
        pipe_list[pl_index++] = trimmed_subcommand;
        sub_command = strtok(NULL, "|");
    }

    // divide commands into subcommands with input output redirection
    char *input_buffer = (char *)calloc(IO_BUFFER, sizeof(char));
    char *output_buffer = (char *)calloc(IO_BUFFER, sizeof(char));

    int exit_status = 0;
    bool is_first = false, is_last = false;
    for (size_t i = 0; i < pl_index; i++) {
        input_buffer = strcpy(input_buffer, output_buffer);
        if (i == 0)
            is_first = true;
        else
            is_first = false;
        if (i == pl_index - 1) {
            is_last = true;
            send_subcommand_forexec(pipe_list[i], input_buffer, output_buffer,
                                    &exit_status, is_first, is_last,
                                    command_toexec->exec_type);
        } else {
            send_subcommand_forexec(pipe_list[i], input_buffer, output_buffer,
                                    &exit_status, is_first, is_last, FG);
        }
        if (exit_status != 0)
            break;
    }
    fprintf(stdout, "%s", output_buffer);

cleanup:
    free(pipe_list);
    free(output_buffer);
    free(input_buffer);
}

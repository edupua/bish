#include "headers.h"

#define MAX_ARGS 32

// executing foreground processes by using the wait command
char *
execute_process(char **argv, FILE *output_fd, char *input_buffer,
                int *exit_status, bool is_first, bool is_last,
                EXEC_TYPE exec_type) {
    char *output_buffer = (char *)calloc(IO_BUFFER, sizeof(char));

    // background process and pipes are not used together
    if (exec_type == BG) {
        is_last = true;
        is_first = true;
    }

    // make a pipe to redirect the output to
    int pipe_fd[2];

    // write to a temporary file to let process read from it
    FILE *temp_file = tmpfile();
    if (fwrite(input_buffer, 1, strlen(input_buffer), temp_file) <
        strlen(input_buffer)) {
        perror(ERROR "fwrite" RESET);
        fclose(temp_file);
    }
    rewind(temp_file); // set pointer to start

    if (pipe(pipe_fd) == -1) {
        perror(ERROR "Output pipe not created\n" RESET);
        exit(EXIT_FAILURE);
    }
    pid_t pid = fork();

    switch (pid) {

    case -1:
        perror(ERROR "Unable to fork a new process\n" RESET);
        break;
    case 0:
        // child process
        close(pipe_fd[0]);

        // assign a group to itself
        setpgid(0, 0);

        // Use dup2 to link the file descriptor of the temporary file to stdin
        if (is_first == false) {
            close(STDIN_FILENO);
            dup2(fileno(temp_file), STDIN_FILENO);
        }
        fclose(temp_file);

        // replace child's output to be to the pipe (write end)
        if (is_last == false || output_fd != NULL) {
            close(STDOUT_FILENO);
            dup2(pipe_fd[1], STDOUT_FILENO);
        }
        close(pipe_fd[1]);

        // finally execute
        default_signals(); // ignore signals
        execvp(argv[0], argv);
        fprintf(stderr, ERROR "failed to execute '%s': " RESET, argv[0]);
        perror(NULL);
        exit(EXIT_FAILURE);
        break;

    default:
        // parent process
        close(pipe_fd[1]);

        // update the foreground values
        if (exec_type == FG) {
            curr_fg = pid;
            strcpy(curr_fg_name, argv[0]);

            // tcsetpgrp sends SIGTTOU to parent and kills it if not used, not
            // ideal in our case
            signal(SIGTTOU, SIG_IGN);
            // give foreground process input access
            tcsetpgrp(STDIN_FILENO, pid);

            waitpid(pid, exit_status, WUNTRACED);

            // see how child exited
            if (WIFSIGNALED(*exit_status)) {
                fprintf(stderr,
                        ERROR "Process (%d) terminated by signal: %d\n" RESET,
                        pid, WTERMSIG(*exit_status));
            } else if (WIFSTOPPED(*exit_status)) {
                append_process(curr_fg_name, pid);
                fprintf(stderr,
                        ERROR "Process (%d) stopped by signal: %d\n" RESET, pid,
                        WSTOPSIG(*exit_status));
            }

            ignore_signals();
            curr_fg = 0;

            // take back input access after exec over
            tcsetpgrp(STDIN_FILENO, getpid());
            signal(SIGTTOU, SIG_DFL);

        } else {
            append_process(argv[0], pid);
            fprintf(stderr, "%d\n", pid);
        }

        // read the contents of output pipe to outputbuffer
        if (read(pipe_fd[0], output_buffer, IO_BUFFER - 1) == -1) {
            fprintf(stderr, ERROR "Error reading from output pipe\n" RESET);
            exit(EXIT_FAILURE);
        }

        close(pipe_fd[0]);
    }

    fclose(temp_file);

    // dump output into the file
    if (output_fd != NULL) {
        fprintf(output_fd, "%s", output_buffer);
        fclose(output_fd);
        strcpy(output_buffer, "");
    }

    return output_buffer;
}

// takes the input file name and returns the file descriptor of it
FILE *
input_file(char *sub_command) {
    if (!strchr(sub_command, '<'))
        return NULL;

    strtok(sub_command, "<");
    char *file_path = strtok(NULL, "<");

    // if both < and >> or > are present
    char *appender = 0;
    if (strstr(file_path, ">>"))
        appender = strdup(">>");
    else if (strchr(file_path, '>'))
        appender = strdup(">");
    else {
        char *trimmed_file_path = trim(file_path);
        FILE *fd = fopen(trimmed_file_path, "r");
        if (fd == NULL)
            simple_error("Error opening input_file, seems to not exist.");
        return fd;
    }
    strtok(file_path, ">");
    char *trimmed_file_path = trim(file_path);
    FILE *fd = fopen(trimmed_file_path, "r");

    if (fd == NULL)
        simple_error("Error opening input_file, seems to not exist.");

    // after you are done with file_path just remove it by doing this
    char *rest = strtok(NULL, ">");
    strcat(sub_command, appender);
    strcat(sub_command, rest);

    free(appender);
    return fd;
}

FILE *
output_file(char *sub_command) {
    char *permission;
    if (strstr(sub_command, ">>"))
        permission = "a";
    else if (strchr(sub_command, '>'))
        permission = "w";
    else
        return NULL;

    strtok(sub_command, ">");
    char *file_path = strtok(NULL, ">");
    char *trimmed_file_path = trim(file_path);
    FILE *fd = fopen(trimmed_file_path, permission);
    if (fd == NULL) {
        simple_error("Error opening file for appending");
    }
    return fd;
}

char *
system_commands(char *sub_command, char *input_buffer, int *exit_status,
                bool is_first, bool is_last, EXEC_TYPE exec_type) {
    FILE *input_fd = input_file(sub_command);
    FILE *output_fd = output_file(sub_command);

    if (input_fd != NULL) {
        is_first = false;
        char finput_buffer[IO_BUFFER];
        while (fgets(finput_buffer, IO_BUFFER, input_fd)) {
            strcat(input_buffer, finput_buffer);
        }
        fclose(input_fd);
    }

    char **argv = (char **)calloc(MAX_ARGS, sizeof(char *));
    parse_arguments(sub_command, argv);
    char *output_buffer =
        execute_process(argv, output_fd, input_buffer, exit_status, is_first,
                        is_last, exec_type);
    free(argv);
    return output_buffer;
}
